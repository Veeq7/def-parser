#include <stdio.h>
#include <stdlib.h>
#include "def_parser.h"

#define ArraySize(X) (sizeof((X)) / sizeof(*(X)))

typedef char bool;

typedef enum {
    UnitType_Small,
    UnitType_Armored,
    UnitType_Titanic,
    UnitType_Count,
} UnitType;

char* unit_type_dict[UnitType_Count] = {
    [UnitType_Small] = "small",
    [UnitType_Armored] = "armored",
    [UnitType_Titanic] = "titanic",
};

typedef enum {
    UnitFlag_Invisible = 0x01,
    UnitFlag_Hidden    = 0x02,
    UnitFlag_Cloaked   = 0x04,
} UnitFlag;

char* unit_flag_dict[] = {
    "invisible",
    "hidden",
    "cloaked",
};

typedef struct {
    char* name;
    int hp;
    int shields;
    int energy;
    int color;
    double speed;
    float splash[2];

    char int8;
    short hex16;
    bool active;
    UnitType type;
    UnitFlag flags;
} Unit;

ParseKey unit_key[] = {
    {.keyword = "unit",     .offset = offsetof(Unit, name),     .type = ParseType_String},
    {.keyword = "health",   .offset = offsetof(Unit, hp),       .type = ParseType_Int},
    {.keyword = "shields",  .offset = offsetof(Unit, shields),  .type = ParseType_Int},
    {.keyword = "energy",   .offset = offsetof(Unit, energy),   .type = ParseType_Int},
    {.keyword = "color",    .offset = offsetof(Unit, color),    .type = ParseType_Hex},
    {.keyword = "speed",    .offset = offsetof(Unit, speed),    .type = ParseType_Double},
    {.keyword = "splash",   .offset = offsetof(Unit, splash),   .type = ParseType_Float,    .count = 2},
    {.keyword = "int8",     .offset = offsetof(Unit, int8),     .type = ParseType_Int,      .size = 1},
    {.keyword = "hex16",    .offset = offsetof(Unit, hex16),    .type = ParseType_Hex,      .size = 2},
    {.keyword = "active",   .offset = offsetof(Unit, active),   .type = ParseType_Bool},
    {.keyword = "flags",    .offset = offsetof(Unit, flags),    .type = ParseType_Flags,    .dict = unit_flag_dict, .dict_size = ArraySize(unit_flag_dict)},
    {.keyword = "type",     .offset = offsetof(Unit, type),     .type = ParseType_Enum,     .dict = unit_type_dict, .dict_size = ArraySize(unit_type_dict)},
};

char* load_file(const char* filename) {
    FILE* file = fopen(filename, "rb");

    fseek(file, 0, SEEK_END);
    size_t size = ftell(file);
    fseek(file, 0, SEEK_SET);

    char* str = malloc(size + 1);
    fread(str, size, 1, file);
    str[size] = 0;

    return str;
}
    
static Unit units[1000];

int main(int argc, char** argv) {
    char* file = load_file("units.def");
    int count = parse_def(file, units, sizeof(Unit), 1000, unit_key, ArraySize(unit_key));
    for (int i = 0; i < count; ++i) {
        Unit* unit = &units[i];
        printf("Unit: %s\n", unit->name);
        printf("H: %i S: %i E: %i\n", unit->hp, unit->shields, unit->energy);
        printf("Color: %x Speed: %lf\n", unit->color, unit->speed);
        printf("Splash: %f %f\n", unit->splash[0], unit->splash[1]);
        printf("Active: %i Type: %s\n", unit->active, unit_type_dict[unit->type]);
        printf("Flags: ");
        for (int i = 0; i < ArraySize(unit_flag_dict); ++i) {
            if (unit->flags & (1 << i)) {
                printf("%s ", unit_flag_dict[i]);
            }
        }
        printf("\nHex16: %x Int8: %i\n\n", (int)unit->hex16, (int)unit->int8);
    }
    printf("Summary: loaded %i units", count);

    free(file);
}