all: parser.exe

parser.exe: main.c def_parser.c
	gcc -Wall -Werror -pedantic -O2 -o $@ $^

clean:
	del *.exe