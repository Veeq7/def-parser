#ifndef DEF_PARSER_H_
#define DEF_PARSER_H_

#include <stddef.h> // for offsetof

typedef enum {
    ParseType_None,
    ParseType_UInt,
    ParseType_Int,
    ParseType_Hex,
    ParseType_Float,
    ParseType_Double,
    ParseType_String,
    ParseType_Bool,
    ParseType_Flags,
    ParseType_Enum,
} ParseType;

typedef struct {
    char* keyword;
    int offset;
    int type;
    int count;

    // Int, Hex
    int size;

    // Enum, Flags
    char** dict;
    int dict_size;
} ParseKey;

int parse_def(char* file, void* structure, int structure_size, int structure_max_count, ParseKey* key, int key_count);

#endif