#include "def_parser.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

typedef struct {
    char* str;
    int size;
} StringView;

static char* sv_alloc_cstr(StringView sv) {
    char* cstr = malloc(sv.size + 1);
    strncpy(cstr, sv.str, sv.size);
    cstr[sv.size] = 0;
    return cstr;
}

static StringView get_next_sv(char** text, char* skip, char* end) {
    while (strchr(skip, (*text)[0]) && (*text)[0] != 0) {
        ++(*text);
    }

    char* keyword = *text;
    int keyword_size = 0;
    while (!strchr(end, (*text)[0]) && (*text)[0] != 0) {
        ++(*text);
        ++keyword_size;
    }

    return (StringView) {
        .str = keyword,
        .size = keyword_size,
    };
}

static StringView get_next_inline_word(char** text) {
    return get_next_sv(text, " \t", " \t\r\n");
}

static StringView get_next_word(char** text) {
    return get_next_sv(text, " \t\r\n", " \t\r\n");
}

static StringView get_next_line(char** text) {
    return get_next_sv(text, " \t\r\n", "\r\n");
}

static void parse_element(char** text, ParseKey key, void* field) {
    if (!key.size) {
        key.size = 4;
    } else if (key.size > 8) {
        key.size = 8;
        assert(!"Key size should never be bigger than 8 bytes!");
    }
    switch (key.type) {
        case ParseType_Int: {
            // tbh we should not allocate here, but doing own strto functions is not a part of this exercise
            char* arg = sv_alloc_cstr(get_next_inline_word(text)); 
            long long l = strtoll(arg, 0, 10);
            memcpy(field, &l, key.size);
            free(arg);
        } break;
        case ParseType_UInt: {
            // tbh we should not allocate here, but doing own strto functions is not a part of this exercise
            char* arg = sv_alloc_cstr(get_next_inline_word(text)); 
            long long l = strtoull(arg, 0, 10);
            memcpy(field, &l, key.size);
            free(arg);
        } break;
        case ParseType_Hex: {
            // tbh we should not allocate here, but doing own strto functions is not a part of this exercise
            char* arg = sv_alloc_cstr(get_next_inline_word(text)); 
            long long l = strtoull(arg, 0, 16);
            memcpy(field, &l, key.size);
            free(arg);
        } break;
        case ParseType_Float: {
            // tbh we should not allocate here, but doing own strto functions is not a part of this exercise
            char* arg = sv_alloc_cstr(get_next_inline_word(text)); 
            float* f = (float*)field;
            *f = strtof(arg, 0);
            free(arg);
        } break;
        case ParseType_Double: {
            // tbh we should not allocate here, but doing own strto functions is not a part of this exercise
            char* arg = sv_alloc_cstr(get_next_inline_word(text)); 
            double* d = (double*)field;
            *d = strtod(arg, 0);
            free(arg);
        } break;
        case ParseType_String: {
            char* arg = sv_alloc_cstr(get_next_line(text)); 
            char** s = (char**)field;
            *s = arg;
        } break;
        case ParseType_Enum: {
            StringView arg = get_next_inline_word(text); 
            for (int i = 0; i < key.dict_size; ++i) {
                if (strncmp(key.dict[i], arg.str, arg.size) == 0) {
                    int* ii = (int*)field;
                    *ii = i;
                    break;
                }
            }
        } break;
        case ParseType_Flags: {
            StringView arg = get_next_inline_word(text); 
            while (arg.size) {
                for (int i = 0; i < key.dict_size; ++i) {
                    if (strncmp(key.dict[i], arg.str, arg.size) == 0) {
                        int* ii = (int*)field;
                        *ii |= 1 << i;
                        break;
                    }
                }
                arg = get_next_inline_word(text);
            }
        } break;
        case ParseType_Bool: {
            char* b = (char*)field;
            *b |= 1;
        } break;
        case ParseType_None: {
        } break;
    }
}

int parse_def(char* file, void* structure, int structure_size, int structure_max_count, ParseKey* key, int key_count) {
    char* file_str = file;
    int structure_index = -1;
    assert(key_count > 0);

    StringView keyword = get_next_word(&file_str);
    while (keyword.size && structure_index + 1 < structure_max_count) {
        for (int i = 0; i < key_count; ++i) {
            if (strncmp(key[i].keyword, keyword.str, keyword.size) == 0) {
                if (i == 0) {
                    ++structure_index;
                    if (structure_index) {
                        structure = (char*)structure + structure_size;
                    }
                } else if (structure_index == -1) {
                    printf("Keyword '%s' before entry declaration '%s'\n", key[i].keyword, key[0].keyword);
                    break;
                }
                
                int count = key->count ? key->count : 1;
                for (int j = 0; j < count; ++j) {
                    parse_element(&file_str, key[i], (char*)structure + key[i].offset);
                }
            }
        }
        keyword = get_next_word(&file_str);
    }

    return structure_index + 1;
}